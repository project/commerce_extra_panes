Module that allows to place one or more nodes as checkout panes for Drupal Commerce.
--Install instructions--
- Install Drupal Commerce and make sure that checkout module and CTools enabled.
- Enable Commerce Extra panes.
- Go to Store > Configuration > Checkout Settings and there click on Checkout Extra Panes tab.
- Select one of the existing nodes of the site in the autocomplete form and add it.
- Click on Checkout Form to select in which checkout phase this should be displayed.

The module provides two tpl files, one for the checkout form phase and the other for the review phase.