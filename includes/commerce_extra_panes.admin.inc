<?php

/**
 * @file
 * Admin pages for commerce extra panes.
 */

/**
 * Form of settings for the extra panes.
 */
function commerce_extra_panes_settings_form($form, &$form_state) {
  $form = array();
  $form['node-pane'] = array(
    '#type' => 'textfield',
    '#title' => t('Select node to use as checkout pane.'),
    '#maxlength' => 60,
    '#autocomplete_path' => 'ctools/autocomplete/node',
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Add pane'));

  $form['extra-panes-list'] = array(
    '#type' =>  'item',
    '#title' => t('List of extra panes.'),
    '#theme' => 'commerce_extra_panes_settings_list',
  );
  return $form;
}

/**
 * Validate function for extra panes settings form.
 */
function commerce_extra_panes_settings_form_validate($form, &$form_state) {
  if ($form_state['values']['node-pane'] == '') {
    form_set_error('node-pane', t('You must select a node for adding a pane.'));
  }
}

/**
 * Submit function for extra panes settings form.
 */
function commerce_extra_panes_settings_form_submit($form, &$form_state) {
  $preg_matches = array();
  $match = preg_match('/\[nid: (\d+)\]/', $form_state['values']['node-pane'], $preg_matches);
  if ($match) {
    $node_id = $preg_matches[1];
    $panes = commerce_extra_panes_get_panes($node_id);
    if (!count($panes)) {
      db_insert('commerce_extra_panes')
        ->fields(array(
          'extra_id' => $node_id,
          'extra_type' => 'node',
          'status' => 1,
        ))
        ->execute();
      drupal_set_message(t('Extra pane saved'));
    }
    else {
      drupal_set_message(t('Pane already added'), 'warning');
    }
  }
}

/**
 * Menu callback -- ask for confirmation of extra pane deletion
 */
function commerce_extra_panes_delete_confirm($form, &$form_state, $extra_id) {
  $extra_pane = current(commerce_extra_panes_get_panes($extra_id));
  $entity = current(entity_load($extra_pane->extra_type, array($extra_id)));
  $form['#entity'] = $entity;
  $form['#extra_id'] = $extra_id;
  return confirm_form($form,
    t('Are you sure you want to delete the pane %title?', array('%title' => $entity->title)),
    'admin/commerce/config/checkout/extra',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute extra pane deletion
 */
function commerce_extra_panes_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $entity = $form['#entity'];
    db_delete('commerce_extra_panes')
      ->condition('extra_id', $form['#extra_id'])
      ->execute();
    drupal_set_message(t('Extra pane %title deleted', array('%title' => $entity->title)));
  }
  $form_state['redirect'] = 'admin/commerce/config/checkout/extra';
}

/**
 * Enable/disable checkout panes.
 */
function commerce_extra_panes_change_status($extra_id) {
  if ($extra_id) {
    $extra_pane = current(commerce_extra_panes_get_panes($extra_id));
    if ($extra_pane->status) {
      //Enabled, let's disable it.
      db_update('commerce_extra_panes')
        ->fields(array(
          'status' => 0,
        ))
        ->condition('extra_id', $extra_id)
        ->execute();
      drupal_set_message(t('Extra pane disabled'));
    }
    else {
      db_update('commerce_extra_panes')
        ->fields(array(
          'status' => 1,
        ))
        ->condition('extra_id', $extra_id)
        ->execute();
      drupal_set_message(t('Extra pane enabled'));
    }
  }
  drupal_goto('admin/commerce/config/checkout/extra');
}
